//
//  AutoDownloadScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 18/09/21.
//

import Foundation
import XCTest

class AutoDownloadScreen: BaseScreen {
    
    private let skipLabel = "SKIP"
    private let enableLabel = "ENABLE"
    private let allow = "Allow"

    
    func skipAutoDownload() -> SplashScreen2 {
        let t = buttonWithLabel(label: skipLabel)
        verifyExistence(element: t, timeout: 3)
        t.tap()
        
        
       
        let r = springboard.alerts.firstMatch.buttons[allow]
        if(r.waitForExistence(timeout: 5)){
            r.tap()
        }
 
 
        return SplashScreen2()
    }
    
    func enableAutoDownload() -> SplashScreen2 {
        let t = buttonWithLabel(label: enableLabel)
        verifyExistence(element: t, timeout: 3)
        t.tap()
        return SplashScreen2()
    }
}
