//
//  BaseScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 17/09/21.
//

import Foundation
import XCTest

class BaseScreen {
    
    let app = XCUIApplication(bundleIdentifier: "com.enphase.enphase-installer")
    let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")

 
    func launchITKClean() -> SplashScreen {
        
        app.launchArguments = ["CLEAN_STATE"]
        if #available(iOS 13.4, *) {
            app.resetAuthorizationStatus(for: .location)
        }
        app.launch()
        return SplashScreen()
    }
    
    
    func buttonWithID(id : String) -> XCUIElement{
        return app.buttons[id].firstMatch
    }
    
    func buttonWithLabel(label : String) -> XCUIElement{
        return app.buttons[label].firstMatch
    }
    
    func textFieldWithLabel(label : String) -> XCUIElement{
        return app.textFields[label].firstMatch
    }
    
    func staticTextWithLabel(label : String) -> XCUIElement{
        return app.staticTexts[label].firstMatch
    }
    
    
    func staticTextWithLabelContains(label : String) -> XCUIElement{
        let predicate = NSPredicate(format: "LABEL CONTAINS '\(label)'")
        return app.staticTexts.matching(predicate).firstMatch
    }
    
    func verifyExistence(element : XCUIElement, timeout : TimeInterval) {
        XCTAssert(element.waitForExistence(timeout: timeout))
    }
    
    func searchFieldWithStartLabel(label : String) -> XCUIElement{
        let predicate = NSPredicate(format: "LABEL BEGINSWITH '\(label)'")
        return app.searchFields.matching(predicate).firstMatch
    }
    
    func textFieldWithStartPlaceholder(placeholder : String) -> XCUIElement {
        let predicate = NSPredicate(format: "placeholderValue BEGINSWITH '\(placeholder)'")
        return app.textFields.matching(predicate).firstMatch
        
    }
    
    func secureTextFieldWithStartPlaceholder(placeholder : String) -> XCUIElement {
        let predicate = NSPredicate(format: "placeholderValue BEGINSWITH '\(placeholder)'")
        return app.secureTextFields.matching(predicate).firstMatch
        
    }
    
    func cellWithText(table : XCUIElement ,text : String) -> XCUIElement {
        //return table.cells.staticTexts[text]
        return table.cells.containing(.staticText, identifier: text).element
    }

    
    func cellWithTextFieldLabel(table : XCUIElement ,label : String) -> XCUIElement {
       //return table.cells.textFields[label]
        return table.cells.containing(.textField, identifier: label).element
    }
    

    
    func tableWithLabel(label : String) -> XCUIElement {
        return app.tables[label]
    }
}
