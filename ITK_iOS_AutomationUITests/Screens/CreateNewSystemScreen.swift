//
//  CreateNewSystemScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 19/09/21.
//

import Foundation
import XCTest

class CreateNewSystemScreen: BaseScreen {
    
    private let systemDetailsLabel = "System Details"
    private let devicesArrayLabel = "Devices & Array"
    private let deleteSystemLabel = "DELETE SYSTEM"
    private let delete = "YES, DELETE"
    private let ok = "OK"

    
    func tapSystemDetails() -> CreateSystemDetailsScreen{
        let t = app.collectionViews.firstMatch
        let cell = cellWithText(table: t, text: systemDetailsLabel)
        verifyExistence(element: cell, timeout: 10)
        cell.tap()
        return CreateSystemDetailsScreen()
    }
    
    func tapDeviceDetails() -> DevicesArrayNumberScreen{
        let t = app.collectionViews.firstMatch
        let cell = cellWithText(table: t, text: devicesArrayLabel)
        cell.wait{ $0.isHittable }
        cell.tap()
        return DevicesArrayNumberScreen()
    }
    
    func tapDevicesArray() -> DevicesArrayScreen{
        let t = app.collectionViews.firstMatch
        let cell = cellWithText(table: t, text: devicesArrayLabel)
        cell.wait{ $0.isHittable }
        cell.tap()
        return DevicesArrayScreen()
    }
    
    func deleteSystem() {
        let t = buttonWithLabel(label: deleteSystemLabel)
        verifyExistence(element: t, timeout: 5)
        t.tap()
        
        let x = buttonWithLabel(label: delete)
        verifyExistence(element: x, timeout: 2)
        x.tap()
        
        let y = buttonWithLabel(label: ok)
        verifyExistence(element: y, timeout: 10)
        y.tap()

    }
    
}
