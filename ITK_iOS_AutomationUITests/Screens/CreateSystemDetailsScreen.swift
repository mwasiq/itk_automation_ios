//
//  CreateSystemDetailsScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 20/09/21.
//

import Foundation
import XCTest

class CreateSystemDetailsScreen: BaseScreen {
    
    private let system = "System Name"
    private let refID = "Installer Reference ID"
    private let firstName = "First Name"
    private let lastName = "Last Name"
    private let email = "Email"
    private let phone = "Phone Number"
    private let country = "Country *"
    private let street1 = "Street Address"
    private let street2 = "Street Address 2"
    private let googleAddress1 = "Search results"
    private let save = "SAVE"
    private let ok = "OK"
    private let csd = "Contract Signed date *"
    private let permitAHJ = "Permit AHJ *"
    private let permitAppDate = "Permit Application date *"
    private let permitApprovalDate = "Permit Approval date *"
    private let hardwareInstallationDate = "Hardware Installation (begin) date *"
    private let done = "Done"
    private let edit = "EDIT"

    
    func editSystemNA() -> CreateNewSystemScreen{

        tapEdit()
        editRefID()
        editFirstName()
        editLastName()
        editEmail()
        editPhone()
        tapSave()
        validateSystemCreatedSuccessfully()

        return CreateNewSystemScreen()
    }
    
    func createSystemNA() -> CreateNewSystemScreen{
        enterSiteName()
        enterRefID()
        //selectOwnerType(type: "Others")
        enterFirstName()
        enterLastName()
        enterEmail()
        enterPhone()
        selectCountry(cntry: "United States")
        selectAddressFromGoogle(address: "Astor Place")
        enterInstallationDetails()
        tapSave()
        validateSystemCreatedSuccessfully()
        
        return CreateNewSystemScreen()
    }
    
    func createSystemEmea() -> CreateNewSystemScreen{
        enterSiteName()
        enterRefID()
        enterFirstName()
        enterLastName()
        enterEmail()
        enterPhone()
        selectCountry(cntry: "Germany")
        selectAddressFromGoogle(address: "Gartnerplatz")
        tapSave()
        validateSystemCreatedSuccessfully()
        
        return CreateNewSystemScreen()

    }
    
    private func tapEdit() {
        let t = buttonWithLabel(label: edit)
        verifyExistence(element: t, timeout: 3)
        t.tap()
    }
    
    
    private func enterSiteName() {
        let siteName = getRandomString(length: 5)
        let cell = textFieldWithLabel(label: system)
        verifyExistence(element: cell, timeout: 2)
        cell.tap()
        cell.typeText(siteName)
    }
    
    private func enterRefID() {
        let ref = getRandomString(length: 4)
        let t = textFieldWithLabel(label: refID)
        t.tap()
        t.typeText(ref)
    }
    
    private func editRefID() {
        let ref = getRandomString(length: 4)
        let t = textFieldWithLabel(label: refID)
        t.clearAndEnterText(text: "")
        t.typeText(ref)
    }
    
    private func selectOwnerType(type : String) {
        let table = app.tables.firstMatch.tables.firstMatch
        let t = cellWithText(table: table, text: type)
        verifyExistence(element: t, timeout: 2)
        t.tap()
    }
    
    private func enterFirstName() {
        let fname = getRandomString(length: 6)
        let t = textFieldWithLabel(label: firstName)
        t.tap()
        t.typeText(fname)
    }
    
    private func editFirstName() {
        let fname = getRandomString(length: 6)
        let t = textFieldWithLabel(label: firstName)
        t.clearAndEnterText(text: "")
        t.typeText(fname)
    }
    
    private func enterLastName() {
        let lname = getRandomString(length: 6)
        let t = textFieldWithLabel(label: lastName)
        t.tap()
        t.typeText(lname)
    }
    
    private func editLastName() {
        let lname = getRandomString(length: 6)
        let t = textFieldWithLabel(label: lastName)
        t.clearAndEnterText(text: "")
        t.typeText(lname)
    }
    
    private func enterEmail() {
        let eml = "\(getRandomString(length: 4)).\(getRandomString(length: 3))@\(getRandomString(length: 3)).com"
        let t = textFieldWithLabel(label: email)
        t.tap()
        t.typeText(eml)
    }
    
    private func editEmail() {
        let eml = "\(getRandomString(length: 4)).\(getRandomString(length: 3))@\(getRandomString(length: 3)).com"
        let t = textFieldWithLabel(label: email)
        t.clearAndEnterText(text: "")
        t.clearAndEnterText(text: "")
        t.typeText(eml)
    }
    
    private func enterPhone() {
        let ph = "5414\(getRandomNumber(length: 6))"
        let t = textFieldWithLabel(label: phone)
        t.tap()
        t.typeText(ph)
        dismissKeyboardIfPresent(app: app)
    }
    
    private func editPhone() {
        let ph = "5414\(getRandomNumber(length: 6))"
        let t = textFieldWithLabel(label: phone)
        t.clearAndEnterText(text: "")
        t.typeText(ph)
        dismissKeyboardIfPresent(app: app)
    }
    
    
    private func selectCountry(cntry : String) {
        let t = staticTextWithLabel(label: country)
        t.tap()
        
        let table2 = app.tables.firstMatch
        let cell2 = cellWithText(table: table2, text: cntry)
        verifyExistence(element: cell2, timeout: 3)
        cell2.tap()
    }
    
    
    private func selectAddressFromGoogle(address : String) {
        let t = textFieldWithLabel(label: street1)
        t.tap()
        
        let searchField = app.searchFields.firstMatch
        verifyExistence(element: searchField, timeout: 2)
        searchField.typeText(address)
        let table2 = tableWithLabel(label: googleAddress1)
        verifyExistence(element: table2, timeout: 5)
        let cell2 = table2.cells.firstMatch
        cell2.tap()
    }
    
    private func tapSave(){
        buttonWithLabel(label: save).tap()
    }
    
    private func validateSystemCreatedSuccessfully(){
        let t = buttonWithLabel(label: ok)
        verifyExistence(element: t, timeout: 6)
        t.tap()
    }
    
    
    private func enterInstallationDetails(){
        let t = textFieldWithLabel(label: csd)
        verifyExistence(element: t, timeout: 2)
        t.tap()
        buttonWithLabel(label: done).tap()
        
        let t1 = textFieldWithLabel(label: permitAHJ)
        t1.tap()
        t1.typeText(getRandomString(length: 4))
        dismissKeyboardIfPresent(app: app)
        
        let t2 = textFieldWithLabel(label: permitAppDate)
        t2.tap()
        buttonWithLabel(label: done).tap()

        let t3 = textFieldWithLabel(label: permitApprovalDate)
        t3.tap()
        buttonWithLabel(label: done).tap()

        let t4 = textFieldWithLabel(label: hardwareInstallationDate)
        t4.forceTapElement()
        buttonWithLabel(label: done).tap()

        
    }
    
    
    
    
}
