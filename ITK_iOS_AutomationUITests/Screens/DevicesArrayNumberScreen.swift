//
//  DevicesArrayNumberScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 21/09/21.
//

import Foundation
import XCTest

class DevicesArrayNumberScreen: BaseScreen {
    
    private let plusId = "plusCircleDk"
    private let envoy = "Envoy"
    private let mi = "Microinverters"
    private let enchargeLabel = "Encharge"
    private let enpowerlabel = "Enpower"
    private let done = "DONE"
    private let ok = "OK"
    private let gotIt = "OK, GOT IT"


    
    func addDevices(envoy : Int, microinverters : Int, encharge : Int, enpower : Int) -> CreateNewSystemScreen {
        if envoy > 0 {
            increaseEnvoyCount(count: envoy)
        }
        
        if microinverters > 0 {
            increaseMICount(count: microinverters)
        }
        
        if encharge > 0 {
            increaseEnchargeCount(count: encharge)
        }
        
        if enpower > 0 {
            increaseEnpowerCount(count: enpower)
        }
        
        tapDone()
        tapOk()
        tapGotIt()
        return CreateNewSystemScreen()
    }

    private func increaseEnvoyCount(count : Int) {
        let envoyCell = cellWithText(table: app.tables.firstMatch, text: envoy)
        let plus = envoyCell.buttons[plusId]
        verifyExistence(element: plus, timeout: 3)
        
        for _ in 1...count {
            plus.tap()
        }
    }
    
    
    private func increaseMICount(count : Int) {
        let envoyCell = cellWithText(table: app.tables.firstMatch, text: mi)
        let plus = envoyCell.buttons[plusId]
        
        for _ in 1...count {
            plus.tap()
        }
    }

    private func increaseEnchargeCount(count : Int) {
        let envoyCell = cellWithText(table: app.tables.firstMatch, text: enchargeLabel)
        let plus = envoyCell.buttons[plusId]
        
        for _ in 1...count {
            plus.tap()
        }
    }
    
    private func increaseEnpowerCount(count : Int) {
        let envoyCell = cellWithText(table: app.tables.firstMatch, text: enpowerlabel)
        let plus = envoyCell.buttons[plusId]
        
        for _ in 1...count {
            plus.tap()
        }
    }
    
    private func tapDone() {
        buttonWithLabel(label: done).tap()
    }
    
    private func tapOk() {
        let t = buttonWithLabel(label: ok)
        verifyExistence(element: t, timeout: 5)
        t.tap()
    }
    
    private func tapGotIt() {
        let t = buttonWithLabel(label: gotIt)
        verifyExistence(element: t, timeout: 5)
        t.tap()
    }
}
