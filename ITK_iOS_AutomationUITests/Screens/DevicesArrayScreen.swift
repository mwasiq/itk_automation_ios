//
//  DevicesArrayScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 21/09/21.
//

import Foundation
import XCTest

class DevicesArrayScreen: BaseScreen {
    
    private let envoy = "Envoy"
    private let microinverters = "Microinverters & Array"
    private let encharge = "Encharge"
    private let enpower = "Enpower"
    
    
    func tapEnvoy() -> ScanEnvoyScreen {
        let t = staticTextWithLabel(label: envoy)
        verifyExistence(element: t, timeout: 5)
        t.tap()
        
        return ScanEnvoyScreen()
    }
    
    func tapMicroinverters() -> ScanMicroinverterScreen {
        let t = staticTextWithLabel(label: microinverters)
        verifyExistence(element: t, timeout: 10)
        t.tap()
        
        return ScanMicroinverterScreen()
    }
    
    func tapEncharges() -> ScanEnchargeScreen {
        let t = staticTextWithLabel(label: encharge)
        verifyExistence(element: t, timeout: 10)
        t.tap()
        
        return ScanEnchargeScreen()
    }
    
    func tapEnpower() -> ScanEnpowerScreen {
        let t = staticTextWithLabel(label: enpower)
        verifyExistence(element: t, timeout: 10)
        t.tap()
        
        return ScanEnpowerScreen()
    }


}
