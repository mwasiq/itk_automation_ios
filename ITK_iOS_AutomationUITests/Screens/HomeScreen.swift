//
//  HomeScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 18/09/21.
//

import Foundation
import XCTest

class HomeScreen: BaseScreen {
    
    private let newSystemLabel = "addNewSystem"
    private let menu = "Menu"

        
    func goToSystem(system : String) -> CreateNewSystemScreen {
        tapSearchField()
        enterSearchField(text: system)
        tapSystem(system: system)
        return CreateNewSystemScreen()
    }
    
    func tapCreateSystem() -> CreateNewSystemScreen {
        let t = buttonWithID(id: newSystemLabel)
        verifyExistence(element: t, timeout: 10)
        t.tap()
        return CreateNewSystemScreen()
    }

    
    func navigateToMenu() -> MenuScreen {
        let t = buttonWithLabel(label: menu)
        verifyExistence(element: t, timeout: 2)
        t.tap()
        return MenuScreen()
    }
    
    private func tapSearchField(){
        app.searchFields.firstMatch.tap()
    }
    
    private func enterSearchField(text : String){
        app.searchFields.firstMatch.typeText(text)
    }
    
    
    private func tapSystem(system : String) {
        let table = app.tables.firstMatch
        let cell = cellWithText(table: table, text: system)
        verifyExistence(element: cell, timeout: 20)
        cell.tap()

    }
}


