//
//  LocationAccessScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 18/09/21.
//

import Foundation
import XCTest

class LocationAccessScreen: BaseScreen {
    
    private let skipLabel = "SKIP"
    private let allowLabel = "ALLOW"
    
    
    func allowLocationPermission() -> AutoDownloadScreen{
        tapAllow()
        allowLocationWhileUsingApp()
        return AutoDownloadScreen()
    }
    
    private func tapAllow() {
        let t = buttonWithLabel(label: allowLabel)
        verifyExistence(element: t, timeout: 15)
        t.tap()
    }
    
    private func allowLocationWhileUsingApp() {
        let t = springboard.alerts.firstMatch.buttons["Allow While Using App"]
        verifyExistence(element: t, timeout: 3)
        t.tap()
        
    }
    
    
    
    
}
