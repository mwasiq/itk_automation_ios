//
//  LoginScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 18/09/21.
//

import Foundation
import XCTest

class LoginScreen: BaseScreen {
    
    private let usernamePlaceholder = "Username"
    private let passwordPlaceholder = "Password"
    private let rememberMeLabel = "Remember me"
    private let loginLabel = "LOGIN"
    private let qa2ButtonLabel = "QA2"

    
    
    func loginToITK(username : String, password : String) -> LocationAccessScreen {
        if isRememberMeChecked(){
            tapRememberMe()
            
            clearUsername()
            clearUsername()
            
            clearPassword()
        }

        enterPassword(password: "<connection>")
        tapLogin()
        tapQA2()
        tapUsername()
        enterUsername(username: username)
        tapPassword()
        enterPassword(password: password)
        tapLogin()
        
        return LocationAccessScreen()
    }
    
    func verifyLoginScreen() {
        let t = textFieldWithStartPlaceholder(placeholder: usernamePlaceholder)
        verifyExistence(element: t, timeout: 5)

    }
    
    private func clearUsername() {
        let t = textFieldWithStartPlaceholder(placeholder: usernamePlaceholder)
        t.clearAndEnterText(text: "")
        dismissKeyboardIfPresent(app: app)
    }
    
    private func clearPassword() {
        let t = secureTextFieldWithStartPlaceholder(placeholder: passwordPlaceholder)
        t.clearAndEnterText(text: "")
        dismissKeyboardIfPresent(app: app)
    }
    
    private func enterUsername(username : String) {
        let t = textFieldWithStartPlaceholder(placeholder: usernamePlaceholder)
        verifyExistence(element: t, timeout: 2)
        t.typeText(username)
    }
    
    private func enterPassword(password : String) {
        let t = secureTextFieldWithStartPlaceholder(placeholder: passwordPlaceholder)
        verifyExistence(element: t, timeout: 2)
        t.tap()
        t.typeText(password)
        t.typeText("\n")
    }
    
    private func tapUsername() {
        textFieldWithStartPlaceholder(placeholder: usernamePlaceholder).tap()
    }
    
    private func tapPassword() {
        secureTextFieldWithStartPlaceholder(placeholder: passwordPlaceholder).tap()
    }
    
    private func isRememberMeChecked() -> Bool {
        let t = textFieldWithStartPlaceholder(placeholder: usernamePlaceholder)
        verifyExistence(element: t, timeout: 2)
        return t.value as! String != "" ? true : false

    }
    
    private func tapLogin() {
        buttonWithLabel(label: loginLabel).tap()
    }
    
    private func tapRememberMe()  {
        buttonWithLabel(label: rememberMeLabel).tap()
    }
    
    private func tapQA2() {
        let t = buttonWithLabel(label: qa2ButtonLabel)
        verifyExistence(element: t, timeout: 2)
        t.tap()
    }
    
    
    
}
