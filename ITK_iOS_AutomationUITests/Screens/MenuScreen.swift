//
//  MenuScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 23/09/21.
//

import Foundation
import XCTest

class MenuScreen: BaseScreen {
    
    private let settings = "Settings"
    private let logout = "Logout"
    private let yes = "YES"
    
    
    func navigateToSettings() -> SettingsScreen {
        
        let t = staticTextWithLabel(label: settings)
        verifyExistence(element: t, timeout: 5)
        t.tap()
        return SettingsScreen()
    }
    
    
    func logOut() -> LoginScreen{
        
        let t = buttonWithLabel(label: logout)
        verifyExistence(element: t, timeout: 5)
        t.tap()
        buttonWithLabel(label: yes).wait{$0.isHittable}.tap()
        return LoginScreen()
    }
    
}


