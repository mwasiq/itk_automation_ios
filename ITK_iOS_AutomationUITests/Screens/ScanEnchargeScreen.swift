//
//  ScanEnchargeScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 21/09/21.
//

import Foundation
import XCTest

class ScanEnchargeScreen: BaseScreen {
    
    private let scanEncharges = "SCAN ENCHARGES"
    private let editSerialNumber = "editSerialNumber"
    private let done = "DONE"
    private let later = "I WILL DO IT LATER"
    private let phaseDropDown = "Select Phase"
    private let retire = "RETIRE"
    private let ok = "OK"
    private let back = "back"
    private let unknown = "Unknown"

    
    func scanEncharge() -> DevicesArrayScreen{
        tapScanEncharges()
        tapEnterManuallyEditIcon()
        let ser = "913\(getRandomNumber(length: 9))"
        enterEnchargeSerialNumber(serialNumber: ser)
        tapDone()
        tapLater()
        return DevicesArrayScreen()
    }
    
    func scanEnchargeEmea() -> DevicesArrayScreen{
        tapScanEncharges()
        tapEnterManuallyEditIcon()
        let ser = "913\(getRandomNumber(length: 9))"
        enterEnchargeSerialNumber(serialNumber: ser)
        tapDone()
        selectPhase(phase: "L1(A)")
        tapLater()
        return DevicesArrayScreen()
    }
    
    func retireEncharge() -> DevicesArrayScreen{

        let t = staticTextWithLabel(label: unknown)
        verifyExistence(element: t, timeout: 5)
        app.tables.firstMatch.cells.firstMatch.wait{$0.isHittable}.tap()
        buttonWithLabel(label: retire).tap()
        buttonWithLabel(label: ok).tap()
        sleep(2)
        buttonWithLabel(label: ok).tap()
        buttonWithLabel(label: back).wait{$0.isHittable}.tap()
        
        return DevicesArrayScreen()
    }
    
    private func tapScanEncharges() {
        buttonWithLabel(label: scanEncharges).tap()
        
    }

    private func tapEnterManuallyEditIcon() {
        buttonWithID(id: editSerialNumber).wait{$0.isHittable}.tap()
    }
    
    
    private func enterEnchargeSerialNumber(serialNumber : String) {
        for (_, c) in serialNumber.enumerated() {
            app.keys[String(c)].tap()
        }
    }
    
    private func tapDone() {
        buttonWithLabel(label: done).tap()
    }
    
    
    private func tapLater() {
        buttonWithLabel(label: later).tap()
    }
    
    private func selectPhase(phase : String) {
        textFieldWithStartPlaceholder(placeholder: phaseDropDown).tap()
        staticTextWithLabel(label: phase).tap()
    }
    
}
