//
//  ScanEnpowerScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 22/09/21.
//

import Foundation
import XCTest

class ScanEnpowerScreen: BaseScreen {
    
    private let editSerialNumber = "editSerialNumber"
    private let done = "DONE"
    private let fullBackup = "Whole Home Backup"
    private let save = "SAVE"
    private let yes = "YES"
    private let ok = "OK"
    private let retire = "RETIRE"

    func scanEnpower() {
        tapEnterManuallyEditIcon()
        let ser = "917\(getRandomNumber(length: 9))"
        enterEnpowerSerialNumber(serialNumber: ser)
        tapDone()
        tapDone()
        tapFullBackUp()
        tapSave()
        tapYes()
        tapOk()
        
    }

    
    func retireEnpower() -> DevicesArrayScreen {
        buttonWithLabel(label: retire).tap()
        buttonWithLabel(label: ok).tap()
        sleep(2)
        buttonWithLabel(label: ok).tap()
        
        return DevicesArrayScreen()

    }
    
    private func tapEnterManuallyEditIcon() {
        buttonWithID(id: editSerialNumber).wait{$0.isHittable}.tap()
    }
    
    private func enterEnpowerSerialNumber(serialNumber : String) {
        for (_, c) in serialNumber.enumerated() {
            app.keys[String(c)].tap()
        }
    }
    
    private func tapDone() {
        buttonWithLabel(label: done).tap()
    }
    
    private func tapFullBackUp() {
        let t = staticTextWithLabel(label: fullBackup)
        verifyExistence(element: t, timeout: 3)
        t.forceTapElement()
    }
    
    
    private func tapSave() {
        buttonWithLabel(label: save).wait{ $0.isEnabled }.tap()
    }
    
    private func tapYes() {
        buttonWithLabel(label: yes).tap()
    }
    
    private func tapOk() {
        buttonWithLabel(label: ok).tap()
    }
}
