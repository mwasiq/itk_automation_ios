//
//  ScanEnvoyScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 21/09/21.
//

import Foundation
import XCTest

class ScanEnvoyScreen: BaseScreen {
    
    private let editSerialNumber = "editSerialNumber"
    private let done = "DONE"
    private let recommendedGP = "Apply recommended grid profile"
    private let currentSelection = "Continue with current selection"
    private let retire = "RETIRE"
    private let ok = "OK"
    
    
    func scanEnvoy() -> DevicesArrayScreen{
        tapEnterManuallyEditIcon()
        let ser = "901\(getRandomNumber(length: 9))"
        enterEnvoySerialNumber(serialNumber: ser)
        tapDone()
        tapDone()
        tapDone2()
        //applyRecommendedGridProfile()
        continueCurrentSelectedGridProfile()
        return DevicesArrayScreen()
        
    }
    
    func retireEnvoy() -> DevicesArrayScreen{
        buttonWithLabel(label: retire).tap()
        sleep(2)
        buttonWithLabel(label: ok).tap()
        sleep(2)
        buttonWithLabel(label: ok).tap()
        sleep(2)
        return DevicesArrayScreen()
        
    }
    private func tapEnterManuallyEditIcon() {
        buttonWithID(id: editSerialNumber).tap()

    }
    
    private func enterEnvoySerialNumber(serialNumber : String) {
        for (_, c) in serialNumber.enumerated() {
            app.keys[String(c)].tap()
        }
        
    
    }
    
    private func tapDone() {
        buttonWithLabel(label: done).tap()
    }
    
    private func tapDone2() {
        let t = buttonWithLabel(label: done)
        t.wait{ $0.isHittable }.tap()
    }
    
    private func applyRecommendedGridProfile() {
        
        let t =  buttonWithLabel(label: recommendedGP)
        verifyExistence(element: t, timeout: 5)
        t.tap()
    }
    
    
    private func continueCurrentSelectedGridProfile() {
        
        let t =  buttonWithLabel(label: currentSelection)
        verifyExistence(element: t, timeout: 5)
        t.tap()
    }
    
    
}
