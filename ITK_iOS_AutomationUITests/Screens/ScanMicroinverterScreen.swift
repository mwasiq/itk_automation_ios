//
//  ScanMicroinverterScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 21/09/21.
//

import Foundation
import XCTest

class ScanMicroinverterScreen: BaseScreen {
    
    private let scanMIs = "SCAN MICROINVERTERS"
    private let editSerialNumber = "editSerialNumber"
    private let done = "DONE"
    private let later = "NO, LATER"
    private let retire = "RETIRE"
    private let ok = "OK"
    private let lastUpdated = "Last updated: "
    private let back = "back"

    func scanMI() -> DevicesArrayScreen {
        tapScanMicros()
        tapEnterManuallyEditIcon()
        let ser = "902\(getRandomNumber(length: 9))"
        enterMicroinverterSerialNumber(serialNumber: ser)
        tapDone()
        tapDone()
        tapLater()
        return DevicesArrayScreen()
        
        
    }
    
    func retireMI() -> DevicesArrayScreen {
        let t = staticTextWithLabel(label: lastUpdated)
        verifyExistence(element: t, timeout: 5)
        app.tables.firstMatch.cells.firstMatch.tables.firstMatch.cells.firstMatch.wait{$0.isHittable}.tap()
        buttonWithLabel(label: retire).tap()
        sleep(2)
        buttonWithLabel(label: ok).tap()
        sleep(2)
        buttonWithLabel(label: ok).tap()
        sleep(2)
        buttonWithLabel(label: back).tap()

        return DevicesArrayScreen()
        
        
    }
    
    private func tapScanMicros() {
        buttonWithLabel(label: scanMIs).tap()
    }
    
    private func tapEnterManuallyEditIcon() {
        buttonWithID(id: editSerialNumber).wait{$0.isHittable}.tap()
        
    }
    
    private func enterMicroinverterSerialNumber(serialNumber : String) {
        for (_, c) in serialNumber.enumerated() {
            app.keys[String(c)].tap()
        }
    }
    
    private func tapDone() {
        buttonWithLabel(label: done).tap()
    }
    
    private func tapLater() {
        buttonWithLabel(label: later).tap()
    }
    
    
    
}
