//
//  SettingsScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 23/09/21.
//

import Foundation
import XCTest

class SettingsScreen: BaseScreen {
    
    
    func validateEnvoySoftwares() {
        
        let r = app.collectionViews.firstMatch.cells.firstMatch
        verifyExistence(element: r, timeout: 20)
        
        r.wait{$0.isEnabled}
        
        let t1 = r.descendants(matching: .staticText).matching(identifier: "Version 04.10.35").element
        verifyExistence(element: t1, timeout: 20)
        
        let t2 = r.descendants(matching: .staticText).matching(identifier: "Version 06.00.98").element
        verifyExistence(element: t2, timeout: 20)
        
        let t3 = r.descendants(matching: .staticText).matching(identifier: "Version 06.01.55").element
        verifyExistence(element: t3, timeout: 20)
        
        //let t4 = r.descendants(matching: .staticText).matching(identifier: "Version 07.00.48").element
        //verifyExistence(element: t4, timeout: 20)
        
        let t5 = r.descendants(matching: .staticText).matching(identifier: "Version 07.01.03").element
        verifyExistence(element: t5, timeout: 20)

    }
    
    
    func validateDefaultGridProfile() {
        
        let r = app.collectionViews.firstMatch.cells.element(boundBy: 1)
        verifyExistence(element: r, timeout: 20)
        
        r.wait{$0.isEnabled}
        
        let t1 = r.descendants(matching: .staticText).matching(identifier: "Default Profile: CA Rule21 201902 VV VW FW (1.2.8)").element
        verifyExistence(element: t1, timeout: 20)
        
    }
}
