//
//  SplashScreen.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 17/09/21.
//

import Foundation
import XCTest

class SplashScreen: BaseScreen {
    
    private let nextButton = "nextBtn"
    
    
    func dismissSplashScreen() -> LoginScreen{
        let button = buttonWithID(id: nextButton)
        verifyExistence(element: button, timeout: 20)
        button.tap()
        return LoginScreen()
    }
    
}
