//
//  SplashScreen2.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 18/09/21.
//

import Foundation
import XCTest

class SplashScreen2: BaseScreen {
    
    private let gotitLabel = "GOT IT"
    
    func dismissSplashScreen2() -> HomeScreen{
        let t = buttonWithLabel(label: gotitLabel)
        verifyExistence(element: t, timeout: 10)
        t.tap()
        
        return HomeScreen()
    }
    

    
    
}
