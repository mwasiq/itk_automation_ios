//
//  ITK_iOS_AutomationUITests.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 17/09/21.
//

import XCTest

class SanitySuite : BaseTest {


    func testLogin() throws {
        _ = BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()

    }
    
    func testSearchSystem() throws {
        _ = BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .goToSystem(system: "WasiqHome4")

    }
    
    func testCreateSystem() throws {
         _ = BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .tapCreateSystem()
            .tapSystemDetails()
            .createSystemNA()

    }
    
    
    func testEditSystem() throws {
        _ = BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .goToSystem(system: "WasiqHome4")
            .tapSystemDetails()
            .editSystemNA()

    }
    
    func testDeleteSystem() throws {
        let site = getRandomString(length: 5)
        let token = oAuthToken()!
        _ = createSite(site: site, oAuthToken: token)
        
         BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .goToSystem(system: site)
            .deleteSystem()

    }
    
    func testAddDevices() throws {
        let site = getRandomString(length: 5)
        let token = oAuthToken()!
        _ = createSite(site: site, oAuthToken: token)
        
         _ = BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .goToSystem(system: site)
            .tapDeviceDetails()
            .addDevices(envoy: 1, microinverters: 1, encharge: 1, enpower: 1)
            .tapDevicesArray()
            .tapEnvoy()
       /*     .scanEnvoy()
            .tapMicroinverters()
            .scanMI()
            .tapEncharges()
            .scanEncharge()
            .tapEnpower()
            .scanEnpower()
 */

 
    }
    
    
    func testRetireDevices() throws {
        let site = getRandomString(length: 5)
        let envoy = "901\(getRandomNumber(length: 9))"
        let pcu = "902\(getRandomNumber(length: 9))"
        let encharge = "913\(getRandomNumber(length: 9))"
        let enpower = "917\(getRandomNumber(length: 9))"

        let token = oAuthToken()!
        let siteID = String(createSite(site: site, oAuthToken: token)!)
        addEnvoy(envoy: envoy, siteID: siteID, token: token)
        addPCU(pcu: pcu, siteID: siteID, token: token)
        addEncharge(enchage: encharge, siteID: siteID, envoy: envoy, token: token)
        addEnpower(enpower: enpower, siteID: siteID, envoy: envoy, token: token)
        
        
        
         _ = BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .goToSystem(system: site)
            .tapDevicesArray()
            .tapEnpower()
            .retireEnpower()
            .tapEncharges()
            .retireEncharge()
            .tapMicroinverters()
            .retireMI()
            .tapEnvoy()
            .retireEnvoy()

    }
    
    
    
    func testEnvoyVersions() throws {
        BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .navigateToMenu()
            .navigateToSettings()
            .validateEnvoySoftwares()

    }
    
    
    func testDefaultGridProfile() throws {
        BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .navigateToMenu()
            .navigateToSettings()
            .validateDefaultGridProfile()

    }
    
    func testSignOut() throws {
        BaseScreen()
            .launchITKClean()
            .dismissSplashScreen()
            .loginToITK(username: "mwasiq@enphaseenergy.com", password: "Enphase@3159")
            .allowLocationPermission()
            .skipAutoDownload()
            .dismissSplashScreen2()
            .navigateToMenu()
            .logOut()
            .verifyLoginScreen()

    }
 
    
 
}
