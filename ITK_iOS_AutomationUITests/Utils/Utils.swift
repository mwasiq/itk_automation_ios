//
//  Utils.swift
//  ITK_iOS_AutomationUITests
//
//  Created by Mohamed Wasiq on 18/09/21.
//

import Foundation
import XCTest

extension XCUIElement {
    
    func forceTapElement() {
        if self.isHittable {
            self.tap()
        }
        else {
            let coordinate: XCUICoordinate = self.coordinate(withNormalizedOffset: CGVector(dx:0.0, dy:0.0))
            coordinate.tap()
        }
    }
    
   /* func clearAndEnterText(app : XCUIApplication, text: String) {
        guard let _ = self.value as? String else {
            XCTFail("Tried to clear and enter text into a non string value")
            return
        }


        for _ in (1...3){
            let value = self.value as! String
            let placeholderValue = self.placeholderValue!
            self.tap()

            if(value == placeholderValue){
                self.typeText(text)
                dismissKeyboardIfPresent(app: app)
                break
            }
            
            let deleteString = value.map { _ in "\u{8}" }.joined(separator: "")
            self.typeText(deleteString)
            
            let value2 = self.value as! String
            let placeholderValue2 = self.placeholderValue!
            
            if(value2 == placeholderValue2){
                self.typeText(text)
                dismissKeyboardIfPresent(app: app)
                break
            }
            
            dismissKeyboardIfPresent(app: app)
          
        }


        
        
    }
    */
    
    func clearAndEnterText(text: String) {
        
        guard let stringValue = self.value as? String else {
            XCTFail("Tried to clear and enter text into a non string value")
            return
        }

        self.tap()

        let deleteString = String(repeating: XCUIKeyboardKey.delete.rawValue, count: stringValue.count)

        self.typeText(deleteString)
        self.typeText(text)
        
    }
    
    static let waitTimeout: TimeInterval = 15

    /// Explicitly wait until either `expression` evaluates to `true` or the `timeout` expires.
    ///
    /// If the condition fails to evaluate before the timeout expires, a failure will be recorded.
    ///
    /// **Example Usage:**
    ///
    /// ```
    /// element.wait(until: { !$0.exists })
    /// element.wait(until: { _ in otherElement.isEnabled }, timeout: 5)
    /// ```
    ///
    /// - Parameters:
    ///   - expression: The expression that should be evaluated before the timeout expires.
    ///   - timeout: The specificied amount of time to check for the given condition to match the expected value.
    ///   - message: An optional description of a failure.
    /// - Returns: The XCUIElement.
    @discardableResult
    func wait(
        until expression: @escaping (XCUIElement) -> Bool,
        timeout: TimeInterval = waitTimeout,
        message: @autoclosure () -> String = "",
        file: StaticString = #file,
        line: UInt = #line
    ) -> Self {
        if expression(self) {
            return self
        }

        let predicate = NSPredicate { _, _ in
            expression(self)
        }

        let expectation = XCTNSPredicateExpectation(predicate: predicate, object: nil)

        let result = XCTWaiter().wait(for: [expectation], timeout: timeout)

        if result != .completed {
            XCTFail(
                message().isEmpty ? "expectation not matched after waiting" : message(),
                file: file,
                line: line
            )
        }

        return self
    }

    /// Explicitly wait until the value of the given `keyPath` equates to `match`.
    ///
    /// If the `keyPath` fails to match before the timeout expires, a failure will be recorded.
    ///
    /// **Example Usage:**
    ///
    /// ```
    /// element.wait(until: \.isEnabled, matches: false)
    /// element.wait(until: \.label, matches: "Downloading...", timeout: 5)
    /// ```
    ///
    /// - Parameters:
    ///   - keyPath: A key path to the property of the receiver that should be evaluated.
    ///   - match: The value that the receivers key path should equal.
    ///   - timeout: The specificied amount of time to check for the given condition to match the expected value.
    ///   - message: An optional description of a failure.
    /// - Returns: The XCUIElement.
    @discardableResult
    func wait<Value: Equatable>(
        until keyPath: KeyPath<XCUIElement, Value>,
        matches match: Value,
        timeout: TimeInterval = waitTimeout,
        message: @autoclosure () -> String = "",
        file: StaticString = #file,
        line: UInt = #line
    ) -> Self {
        wait(
            until: { $0[keyPath: keyPath] == match },
            timeout: timeout,
            message: message(),
            file: file,
            line: line
        )
    }

    /// Explicitly wait until the value of the value of the given `keyPath` equals `true`.
    ///
    /// If the `keyPath` fails to match before the timeout expires, a failure will be recorded.
    ///
    /// **Example Usage:**
    ///
    /// ```
    /// element.wait(until: \.exists)
    /// element.wait(until: \.exists, timeout: 5)
    /// ```
    ///
    /// - Parameters:
    ///   - keyPath: The KeyPath that represents which property of the receiver should be evaluated.
    ///   - timeout: The specificied amount of time to check for the given condition to match the expected value.
    ///   - message: An optional description of a failure.
    /// - Returns: The XCUIElement.
    func wait(
        until keyPath: KeyPath<XCUIElement, Bool>,
        timeout: TimeInterval = waitTimeout,
        message: @autoclosure () -> String = "",
        file: StaticString = #file,
        line: UInt = #line
    ) -> Self {
        wait(
            until: keyPath,
            matches: true,
            timeout: timeout,
            message: message(),
            file: file,
            line: line
        )
    }

    /// Explicitly wait until the value of the given `keyPath` does not equate to `match`.
    ///
    /// If the `keyPath` fails to not match before the timeout expires, a failure will be recorded.
    ///
    /// **Example Usage:**
    ///
    /// ```
    /// element.wait(until: \.isEnabled, doesNotMatch: false)
    /// element.wait(until: \.label, doesNotMatch: "Downloading...", timeout: 5)
    /// ```
    ///
    /// - Parameters:
    ///   - keyPath: A key path to the property of the receiver that should be evaluated.
    ///   - match: The value that the receivers key path should not equal.
    ///   - timeout: The specificied amount of time to check for the given condition to match the expected value.
    ///   - message: An optional description of a failure.
    /// - Returns: The XCUIElement.
    @discardableResult
    func wait<Value: Equatable>(
        until keyPath: KeyPath<XCUIElement, Value>,
        doesNotMatch match: Value,
        timeout: TimeInterval = waitTimeout,
        message: @autoclosure () -> String = "",
        file: StaticString = #file,
        line: UInt = #line
    ) -> Self {
        wait(
            until: { $0[keyPath: keyPath] != match },
            timeout: timeout,
            message: message(),
            file: file,
            line: line
        )
    }
}




func dismissKeyboardIfPresent(app: XCUIApplication) {
    if app.keyboards.element(boundBy: 0).exists {
        if UIDevice.current.userInterfaceIdiom == .pad {
            app.keyboards.buttons["Hide keyboard"].tap()
        } else {
            app.toolbars.buttons["Done"].tap()
        }
    }
}

func currentTimeInMilliSeconds() -> Int
    {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }

func oAuthToken() -> String? {
    
    struct Token : Codable{
        let access_token : String
    }

    var oAuthToken : String?
    let semaphore = DispatchSemaphore (value: 0)

    var request = URLRequest(url: URL(string: "https://api-qa2.enphaseenergy.com/oauth/token?grant_type=password&password=Enphase@3159&username=mwasiq@enphaseenergy.com")!, timeoutInterval: Double.infinity)
    
    request.addValue("Basic aW5zdGFsbGVyLXRvb2xraXQ6cjBQaTEwemN4cGlKSVRaQ0V2bUwyR3h6MzZrS2RWcDBvUjZZT2pYdQ==", forHTTPHeaderField: "Authorization")
    request.httpMethod = "POST"

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        oAuthToken = nil
        semaphore.signal()
        return
      }
        
        let decoder = JSONDecoder()
        let product = try? decoder.decode(Token.self, from: data)
        
        if let product = product{
            let token = product.access_token
            oAuthToken = token
        }

        else {oAuthToken = nil}
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()

    return oAuthToken
    
}


func getSystemFromEnvoyNumber(EnvoySerialNumber : String, oAuthToken : String) -> Int? {
    
    struct System : Codable{
        let system_id : Int
    }
    
    var de : Int?
    
    let semaphore = DispatchSemaphore (value: 0)

    var request = URLRequest(url: URL(string: "https://api-qa2.enphaseenergy.com/api/v4/systems/retrieve_system_id?serial_num=\(EnvoySerialNumber)")!,timeoutInterval: Double.infinity)
    request.addValue("b4d162b4cd5334c7b63d1c75f8ed1650", forHTTPHeaderField: "key")
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("Bearer \(oAuthToken)", forHTTPHeaderField: "Authorization")

    request.httpMethod = "GET"

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print(String(describing: error))
        de = nil
        semaphore.signal()
        return
      }
      print(String(data: data, encoding: .utf8)!)
        
        
        let decoder = JSONDecoder()
        let product = try? decoder.decode(System.self, from: data)
        
        if let product = product{
            let token = product.system_id
            de = token
        }

        else {de = nil}
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()

    return de
    
}


func retireEnvoy(EnvoySerialNumber : String, systemID : Int, oAuthToken : String) {
    

    let semaphore = DispatchSemaphore (value: 0)

    let parameters = "[\"\(EnvoySerialNumber)\"]"
    let postData = parameters.data(using: .utf8)

    var request = URLRequest(url: URL(string: "https://api-qa2.enphaseenergy.com/api/v4/systems/\(systemID)/envoys")!,timeoutInterval: Double.infinity)
    request.addValue("33d4cec7b72267778b67d0bacded2753", forHTTPHeaderField: "key")
    request.addValue("Bearer \(oAuthToken)", forHTTPHeaderField: "Authorization")
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")

    request.httpMethod = "DELETE"
    request.httpBody = postData

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print(String(describing: error))
        semaphore.signal()
        return
      }
      print(String(data: data, encoding: .utf8)!)
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()

    
}


func getRandomString(length : Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyz"
     return String((0..<length).map{ _ in letters.randomElement()! })
}

func getRandomNumber(length : Int) -> String {
    let letters = "0123456789"
     return String((0..<length).map{ _ in letters.randomElement()! })
}

func createSite(site : String, oAuthToken : String) -> Int? {

    struct System : Codable{
        let system_id : Int
    }
    
    var de : Int?

    
    let eml1 = getRandomString(length: 4)
    let eml2 = getRandomString(length: 4)

    let fname = getRandomString(length: 6)
    let lname = getRandomString(length: 6)
    let ph = "5414\(getRandomNumber(length: 6))"
    let currentmillis = String(currentTimeInMilliSeconds())

    let semaphore = DispatchSemaphore (value: 0)

    let parameters = "system=%7B%22address%22%3A%7B%22city%22%3A%22New%20York%22%2C%22country%22%3A%22US%22%2C%22state%22%3A%22NY%22%2C%22street1%22%3A%22Astor%20Place%22%2C%22street2%22%3A%22%22%2C%22postal_code%22%3A%2210003%22%2C%22latitude%22%3A0%2C%22longitude%22%3A0%7D%2C%22batteries%22%3A%5B%5D%2C%22battery_arrays%22%3A%5B%5D%2C%22envoys%22%3A%5B%5D%2C%22expected_acb_count%22%3A0%2C%22expected_envoy_count%22%3A0%2C%22expected_meter_count%22%3A0%2C%22expected_pcu_count%22%3A0%2C%22expected_nsr_count%22%3A0%2C%22internet_connection%22%3A%22none%22%2C%22is_lease%22%3Afalse%2C%22meters%22%3A%5B%5D%2C%22inverters%22%3A%5B%5D%2C%22arrays%22%3A%5B%5D%2C%22system_name%22%3A%22\(site)%22%2C%22owner%22%3A%7B%22email%22%3A%22\(eml1)%40\(eml2).com%22%2C%22first_name%22%3A%22\(fname)%22%2C%22last_name%22%3A%22\(lname)%22%2C%22phone%22%3A%22\(ph)%22%7D%2C%22nsrs%22%3A%5B%5D%2C%22reference%22%3A%22Yddd%22%2C%22stage%22%3A1%2C%22system_type%22%3A%22residential%22%2C%22check_existing_owner%22%3Atrue%2C%22requested_profile%22%3A%22agf%3A5da3aa013da64a3fee2d7084%22%2C%22requested_profile_name%22%3A%22CA%20Rule21%20201902%20VV%20VW%20FW%20(1.2.8)%22%2C%22system_id%22%3A-1%2C%22permit_details%22%3A%7B%22solar%22%3A%7B%22contract_signed_date%22%3A\(currentmillis)%2C%22hardware_installation_date%22%3A\(currentmillis)%2C%22permit_ahj%22%3A%22fxycj%22%2C%22permit_complete_date%22%3A\(currentmillis)%2C%22permit_submission_date%22%3A\(currentmillis)%7D%2C%22storage%22%3A%7B%22contract_signed_date%22%3A\(currentmillis)%2C%22hardware_installation_date%22%3A\(currentmillis)%2C%22permit_ahj%22%3A%22fxycj%22%2C%22permit_complete_date%22%3A\(currentmillis)%2C%22permit_submission_date%22%3A\(currentmillis)%7D%7D%7D"
    let postData =  parameters.data(using: .utf8)

    var request = URLRequest(url: URL(string: "https://api-qa2.enphaseenergy.com/api/v4/activations")!,timeoutInterval: Double.infinity)
    request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.addValue("Bearer \(oAuthToken)", forHTTPHeaderField: "Authorization")

    request.httpMethod = "POST"
    request.httpBody = postData

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print(String(describing: error))
        de = nil
        semaphore.signal()
        return
      }
      print(String(data: data, encoding: .utf8)!)
        
        let decoder = JSONDecoder()
        let product = try? decoder.decode(System.self, from: data)
        
        if let product = product{
            let token = product.system_id
            de = token
        }

        else {de = nil}
        
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()
    return de
}


func addEnvoy(envoy : String, siteID : String, token : String) {

    let semaphore = DispatchSemaphore (value: 0)

    let parameters = "[{\"grid_profile_id\":\"agf:5da3aa013da64a3fee2d7084\",\"part_num\":\"800-00555-r01\",\"phase_num\":0,\"serial_num\":\"\(envoy)\"}]"
    let postData = parameters.data(using: .utf8)

    var request = URLRequest(url: URL(string: "https://api-qa2.enphaseenergy.com/api/v3/systems/\(siteID)/envoys")!,timeoutInterval: Double.infinity)
    request.addValue("33d4cec7b72267778b67d0bacded2753", forHTTPHeaderField: "key")
    request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")

    request.httpMethod = "PUT"
    request.httpBody = postData

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print(String(describing: error))
        semaphore.signal()
        return
      }
      print(String(data: data, encoding: .utf8)!)
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()

}


func addPCU(pcu : String, siteID : String, token : String) {

    let semaphore = DispatchSemaphore (value: 0)

    let parameters = "[{\"part_num\":\"800-00107-r01\",\"phase_num\":0,\"serial_num\":\"\(pcu)\"}]"
    let postData = parameters.data(using: .utf8)

    var request = URLRequest(url: URL(string: "https://api-qa2.enphaseenergy.com/api/v3/systems/\(siteID)/pcus")!,timeoutInterval: Double.infinity)
    request.addValue("33d4cec7b72267778b67d0bacded2753", forHTTPHeaderField: "key")
    request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")

    request.httpMethod = "PUT"
    request.httpBody = postData

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print(String(describing: error))
        semaphore.signal()
        return
      }
      print(String(data: data, encoding: .utf8)!)
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()

}

func addEncharge(enchage : String, siteID : String, envoy : String, token : String) {

    let semaphore = DispatchSemaphore (value: 0)

    let parameters = "system=%7B%22encharge%22%3A%5B%7B%22encharge_serial_numbers%22%3A%5B%22\(enchage)%22%5D%2C%22envoy_serial_number%22%3A%22\(envoy)%22%2C%22phase_values%22%3A%7B%7D%7D%5D%2C%22envoy_serial_numbers%22%3A%5B%22\(envoy)%22%5D%7D"
    let postData =  parameters.data(using: .utf8)

    var request = URLRequest(url: URL(string: "https://api-qa2.enphaseenergy.com/api/v4/activations/\(siteID)")!,timeoutInterval: Double.infinity)
    request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

    request.httpMethod = "PUT"
    request.httpBody = postData

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print(String(describing: error))
        semaphore.signal()
        return
      }
      print(String(data: data, encoding: .utf8)!)
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()

}


func addEnpower(enpower : String, siteID : String, envoy : String, token : String) {

    let semaphore = DispatchSemaphore (value: 0)

    let parameters = "system=%7B%22backup_type%22%3A2%2C%22partial_backup_type%22%3A0%2C%22enpower%22%3A%5B%7B%22enpower_serial_numbers%22%3A%5B%22\(enpower)%22%5D%2C%22envoy_serial_number%22%3A%22\(envoy)%22%7D%5D%2C%22envoy_serial_numbers%22%3A%5B%22\(envoy)%22%5D%7D"
    let postData =  parameters.data(using: .utf8)

    var request = URLRequest(url: URL(string: "https://api-qa2.enphaseenergy.com/api/v4/activations/\(siteID)")!,timeoutInterval: Double.infinity)
    request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

    request.httpMethod = "PUT"
    request.httpBody = postData

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print(String(describing: error))
        semaphore.signal()
        return
      }
      print(String(data: data, encoding: .utf8)!)
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()

}


